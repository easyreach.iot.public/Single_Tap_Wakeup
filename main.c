/**************************************************************************//**
 * 
 *	This is a main file that hosts the logic that senses a sigle tap.
 *	Gyrosensor module LSM6DSL is configured to do so.
 *	I2C0 interface has been used to configure LSM6DSL module.
 *****************************************************************************/
 
#include"stdio.h"
#include"string.h"
#include"Nano100Series.h"

#define SLAVE_ADDRESS 0x6B

uint8_t g_au8MasterTxData[]={

	0x10,0x60,
	0x58,0x8f,
	0x59,0x89,
	0x5a,0x06,
	0x5b,0x00,
	0x5f,0x40

};

uint8_t g_u8SlvData[256];
uint8_t g_u8MasterRxData;
uint8_t g_au8SlaveRxData[3];
volatile uint8_t g_u8MasterDataLen=0;
uint8_t g_u8SlaveDataLen;
volatile uint8_t g_u8EndFlag = 0;
int number_of_i2c_commands=sizeof(g_au8MasterTxData)/sizeof(g_au8MasterTxData[0]);		

int tap_status=0;

/*
*	Interrupt handler for Port B Pin 5
* This interrupt suggests that LSM6DSL has detected a single tap.
*/
void GPABC_IRQHandler(void)
{
    /* To check if PB.5 interrupt occurred */
    if(GPIO_GET_INT_FLAG(PB, BIT15)) {
        GPIO_CLR_INT_FLAG(PB, BIT15);
        printf("PB.15 INT occurred. \n");
				tap_status=1;
    } else {
        /* Un-expected interrupt. Just clear all PORTA, PORTB, PORTC interrupts */
        PA->ISRC = PA->ISRC;
        PB->ISRC = PB->ISRC;
        PC->ISRC = PC->ISRC;
        printf("Un-expected interrupts. \n");
    }
}	

int check_tap_status(void)
{
	return tap_status;
}

/*
*	This function will initialize the system clocks and GPIO multiplexed functions so that system can comunicate over UART0, I2C0.
*/
void init(void)
{
	SYS_UnlockReg();
	
	CLK_SetHCLK(CLK_CLKSEL0_HCLK_S_HIRC,CLK_HCLK_CLK_DIVIDER(1));
	
	CLK_EnableXtalRC(CLK_PWRCTL_HIRC_EN);
	CLK_WaitClockReady(CLK_CLKSTATUS_HIRC_STB_Msk);
	
	CLK_SetHCLK(CLK_CLKSEL0_HCLK_S_HIRC,CLK_HCLK_CLK_DIVIDER(1));
	
	CLK_SetModuleClock(UART0_MODULE,CLK_CLKSEL1_UART_S_HIRC,CLK_UART_CLK_DIVIDER(1));
	CLK_SetModuleClock(TMR1_MODULE,CLK_CLKSEL1_TMR1_S_HIRC,0);
	CLK_SetModuleClock(I2C0_MODULE,0,0);
	
	CLK_EnableModuleClock(UART0_MODULE);
	CLK_EnableModuleClock(TMR1_MODULE);
	CLK_EnableModuleClock(I2C0_MODULE);
	
	SYS->PB_L_MFP &= ~(SYS_PB_L_MFP_PB0_MFP_Msk | SYS_PB_L_MFP_PB1_MFP_Msk);
	SYS->PB_L_MFP |= (SYS_PB_L_MFP_PB1_MFP_UART0_TX | SYS_PB_L_MFP_PB0_MFP_UART0_RX);
	
	SYS->PA_H_MFP &= ~(SYS_PA_H_MFP_PA8_MFP_Msk | SYS_PA_H_MFP_PA9_MFP_Msk);
	SYS->PA_H_MFP |= (SYS_PA_H_MFP_PA9_MFP_I2C0_SCL | SYS_PA_H_MFP_PA8_MFP_I2C0_SDA);
	
	SYS_LockReg();
}

/*
* This function will configure LSM6DSL for a single tap detection.
* Please change the values of g_au8MasterTxData array and number_of_i2c_commands to configure it for some other functionality.
*/
void I2C_Communicate()
{
	int i=0;
	
	I2C_Open(I2C0,400000);	
	
	while(i<number_of_i2c_commands)
	{
		I2C_SET_CONTROL_REG(I2C0,I2C_STA);
		while(I2C_GetStatus(I2C0)!=0x08);
		printf("I2C0 started\n");
		
		I2C_SetData(I2C0,SLAVE_ADDRESS<<1);
		I2C_SET_CONTROL_REG(I2C0,I2C_SI);
		while(I2C_GetStatus(I2C0)!=0x18);
		
		printf("Writitng 0x%02x\n",g_au8MasterTxData[i]);
		I2C_SetData(I2C0,g_au8MasterTxData[i++]);
		I2C_SET_CONTROL_REG(I2C0,I2C_SI);
		while(I2C_GetStatus(I2C0)!=0x28);
		printf("Writitng 0x%02x\n",g_au8MasterTxData[i]);
		I2C_SetData(I2C0,g_au8MasterTxData[i++]);
		I2C_SET_CONTROL_REG(I2C0,I2C_SI);
		while(I2C_GetStatus(I2C0)!=0x28);
		
		I2C_SET_CONTROL_REG(I2C0,I2C_STO | I2C_SI);
		while(I2C_GetStatus(I2C0)!=0xf8);
		printf("I2C0 stopped\n");
		TIMER_Delay(TIMER1,500000);
	}
	
	i=0;
	
	printf("Recieving now\n\n\n");
	
	while(i<number_of_i2c_commands)
	{
		I2C_SET_CONTROL_REG(I2C0,I2C_STA);
		while(I2C_GetStatus(I2C0)!=0x08);
		printf("I2C0 started\n");
		
		I2C_SetData(I2C0,SLAVE_ADDRESS<<1);
		I2C_SET_CONTROL_REG(I2C0,I2C_SI);
		while(I2C_GetStatus(I2C0)!=0x18);
		
		printf("Writitng 0x%02x\n",g_au8MasterTxData[i]);
		I2C_SetData(I2C0,g_au8MasterTxData[i]);
		i+=2;
		I2C_SET_CONTROL_REG(I2C0,I2C_SI);
		while(I2C_GetStatus(I2C0)!=0x28);
		I2C_SET_CONTROL_REG(I2C0,I2C_STA | I2C_SI);
		while(I2C_GetStatus(I2C0)!=0x10);
		I2C_SET_DATA(I2C0, (SLAVE_ADDRESS << 1) | 0x01);
    I2C_SET_CONTROL_REG(I2C0, I2C_SI);
		while(I2C_GetStatus(I2C0)!=0x40);
		I2C_SET_CONTROL_REG(I2C0, I2C_SI);
		while(I2C_GetStatus(I2C0)!=0x58);
		g_u8MasterRxData = I2C_GET_DATA(I2C0);
		printf("Recieved : 0x%02x\n",g_u8MasterRxData);
		I2C_SET_CONTROL_REG(I2C0, I2C_STO | I2C_SI);
		while(I2C_GetStatus(I2C0)!=0xf8);
		printf("I2C0 stopped\n");
		TIMER_Delay(TIMER1,500000);
	}
	
	printf("I2C0 status = %x\n",I2C_GetStatus(I2C0));
	printf("Returning from I2C_Communicate()\n");
}

/*
*	This is the main function .
*/
int main()
{
	init();
	
	GPIO_SetMode(PB, BIT15, GPIO_PMD_INPUT);
	

	UART_Open(UART0,115200);

	printf("Starting I2C\n");
	
	I2C_Communicate();
	printf("I2C Done\n");
	
	GPIO_EnableInt(PB, 15, GPIO_INT_BOTH_EDGE);
  NVIC_EnableIRQ(GPABC_IRQn);

	while(1)
	{
		printf("Trying to power down\n");
		SYS_UnlockReg();
		CLK_PowerDown();
		SYS_LockReg();

		if(check_tap_status())
		{
			printf("TAP has been detected!\n");
			tap_status=0;
		}
	}
}
